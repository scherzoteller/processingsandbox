package org.scherzoteller.processing.commons;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageBuf {
	public int[] pixels;
	public int width;
	public int height;
	public boolean hasAlphaChannel;
	public ImageBuf(int[] pixels, int width, int height, boolean hasAlphaChannel) {
		this.pixels = pixels;
		this.width = width;
		this.height = height;
		this.hasAlphaChannel = hasAlphaChannel;
	}
	public static int getPixelIndex(int y, int x, int width) {
		return (y * width) + x;
	}
	
	public int getPixelIndex(int y, int x) {
		return (y * width) + x;
	}
	
	public void setPixel(int x, int y, int color){
		this.pixels[getPixelIndex(y,x)] = color;
	}
	
	public int getPixel(int x, int y){
		return this.pixels[getPixelIndex(y,x)];
	}
	private static BufferedImage getImage(String filename){
		try {
			return ImageIO.read(new File(ImageBuf.class.getResource("/"+filename).getFile()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public ImageBuf(String filename) {
		this(getImage(filename));
	}

	/**
	 * Note that image is reverted pixel should be accessed as resultArray[y][x]
	 * FIXME can be sometimes buggy: see ko_pattern.png
	 * 
	 * @param image
	 * @return
	 */
	public ImageBuf(BufferedImage image) {
		
		this.width = image.getWidth();
		this.height = image.getHeight();
		this.hasAlphaChannel = image.getAlphaRaster() != null;
		this.pixels = new int[height*width];

		DataBuffer dataBuffer = image.getRaster().getDataBuffer();

		if (dataBuffer instanceof DataBufferByte) {
			convertTo2DWithoutUsingGetRGB(((DataBufferByte) dataBuffer).getData());
		} else if (dataBuffer instanceof DataBufferInt) {
			convertTo2DWithoutUsingGetRGB(((DataBufferInt) dataBuffer).getData());
		}
	}

	private void convertTo2DWithoutUsingGetRGB(final int[] imPixels) {
		for (int pixel = 0, row = 0, col = 0; pixel < imPixels.length; pixel++) {
			setPixel(col, row, imPixels[pixel]);
			col++;
			if (col == width) {
				col = 0;
				row++;
			}
		}
	}

	private void convertTo2DWithoutUsingGetRGB(final byte[] imPixels) {
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0, row = 0, col = 0; pixel + 3 < imPixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += (((int) imPixels[pixel] & 0xff) << 24); // alpha
				argb += ((int) imPixels[pixel + 1] & 0xff); // blue
				argb += (((int) imPixels[pixel + 2] & 0xff) << 8); // green
				argb += (((int) imPixels[pixel + 3] & 0xff) << 16); // red
				setPixel(col, row, argb);

				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0, row = 0, col = 0; pixel + 2 < imPixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += -16777216; // 255 alpha
				argb += ((int) imPixels[pixel] & 0xff); // blue
				argb += (((int) imPixels[pixel + 1] & 0xff) << 8); // green
				argb += (((int) imPixels[pixel + 2] & 0xff) << 16); // red
				setPixel(col, row, argb);
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		}
	}
}
