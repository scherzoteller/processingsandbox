package org.scherzoteller.processing.commons;

import java.awt.image.BufferedImage;
import java.util.stream.IntStream;

public class BlurableImageBuf extends ImageBuf {
	public static class BlurFilter {
		public int[] filter;
		public int filterWidth;

		public BlurFilter(int[] filter, int filterWidth) {
			super();
			this.filter = filter;
			this.filterWidth = filterWidth;
		}
	}

	public static final BlurFilter THREE = new BlurFilter(new int[] { //
			1, 2, 1, //
			2, 4, 2, //
			1, 2, 1 //
	}, 3);
	
	public static final BlurFilter FIVE = new BlurFilter(new int[] { //
			1, 16, 49, 16, 1, //
			16, 256, 676, 256, 16, //
			49, 676, 1681, 676, 49, //
			16, 256, 676, 256, 16, //
			1, 16, 49, 16, 1 //
	}, 5);
	
	public static final BlurFilter SEVEN = new BlurFilter(new int[] { //
			1, 4, 7, 16, 7, 4, 1, //
			4, 16, 26, 41, 26, 16, 4, //
			7, 26, 41, 79, 41, 26, 7, //
			4, 16, 26, 41, 26, 16, 4, //
			1, 4, 7, 16, 7, 4, 1 //
	}, 7);

	public BlurableImageBuf(BufferedImage image) {
		super(image);
	}

	public BlurableImageBuf(int[] pixels, int width, int height, boolean hasAlphaChannel) {
		super(pixels, width, height, hasAlphaChannel);
	}

	public BlurableImageBuf(String filename) {
		super(filename);
	}

	public static ImageBuf blur(ImageBuf image) {
		return blur(image); // FIXME
	}

	public static ImageBuf blur(ImageBuf image, BlurFilter filter) {
		final int width = image.width;
		final int height = image.height;
		int[] input = image.pixels;
		int[] output = new int[input.length];
		blur(filter, width, height, input, output);
		return new ImageBuf(output, width, height, image.hasAlphaChannel);
	}

	public void blur() {
		blur(SEVEN);
	}
	public void blur(BlurFilter filter) {
		BlurableImageBuf.blur(filter, width, height, this.pixels, this.pixels);
	}

	private static void blur(BlurFilter filter, final int width, final int height, int[] input, int[] output) {
		if (filter.filter.length % filter.filterWidth != 0) {
			throw new IllegalArgumentException("filter contains a incomplete row");
		}

		final int sum = IntStream.of(filter.filter).sum();

		final int pixelIndexOffset = width - filter.filterWidth;
		final int centerOffsetX = filter.filterWidth / 2;
		final int centerOffsetY = filter.filter.length / filter.filterWidth / 2;

		// apply filter
		for (int h = height - filter.filter.length / filter.filterWidth + 1, w = width - filter.filterWidth + 1, y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				int r = 0;
				int g = 0;
				int b = 0;
				for (int filterIndex = 0, pixelIndex = y * width + x; filterIndex < filter.filter.length; pixelIndex += pixelIndexOffset) {
					for (int fx = 0; fx < filter.filterWidth; fx++, pixelIndex++, filterIndex++) {
						int col = input[pixelIndex];
						int factor = filter.filter[filterIndex];

						// sum up color channels seperately
						r += ((col >>> 16) & 0xFF) * factor;
						g += ((col >>> 8) & 0xFF) * factor;
						b += (col & 0xFF) * factor;
					}
				}
				r /= sum;
				g /= sum;
				b /= sum;
				// combine channels with full opacity
				output[x + centerOffsetX + (y + centerOffsetY) * width] = (r << 16) | (g << 8) | b | 0xFF000000;
			}
		}
	}

}
