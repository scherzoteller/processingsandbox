package org.scherzoteller.processing.sandbox;

import processing.core.PApplet;


public class Plasma extends PApplet {

	int palette[] = new int[256]; // Ein Array mit 256 Elementen, jedes Element
									// ist ein color.

	
	@Override
	public void settings() {
		super.settings();
//		size(1280, 1024, P2D);
		size(800, 600, P2D);
	}
	
	public void setup() {
		background(51);
		noStroke();
		// smooth();

		// create color palette
		int i, r, g, b;
		// between 0..100 fade black to red
		for (i = 0; i < 100; i++) {
			r = i * 255 / 100;
			g = 0;
			b = 0;
			palette[i] = color(r, g, b);
		}
		// between 100..150 fade red to yellow
		for (; i < 150; i++) {
			r = 255;
			g = (i - 100) * 255 / 50;
			b = 0;
			palette[i] = color(r, g, b);
		}
		// between 150..255 fade yellow to blue
		for (; i <= 255; i++) {
			r = 255 - ((i - 150) * 255 / 106);
			g = 255 - ((i - 150) * 255 / 106);
			b = (i - 150) * 255 / 106;
			palette[i] = color(r, g, b);
		}
	}

	float deg;
	float rad;

	int currentColor;

	int currentPixel;
	float fColor;

	int time;

	float value;
	
	final int minX=500, maxX=750, minY=250, maxY=500;
	

	public void draw() {
		loadPixels();
		for (int y = minY; y < maxY; y++) {
			// draw line
			for (int x = minX; x < maxX; x++) {

				// 4 different sinus functions modulating
				fColor = ((sin(x / 18.0f + time / 99.0f) + cos(y / 39.0f + time / 30.0f)) + sin(x / 39.0f - time / 40.0f) + cos(y / 15.0f - time / 40.0f)) / 4;

				/*
				 * variante fColor = ((sin (x/15.0 + time/99.0) * cos (y/39.0 +
				 * time/30.0)) + sin (x/39.0 - time/40.0) * cos (y/15.0 -
				 * time/40.0)) / 4;
				 */

				// normalize to 0..255.
				fColor = ((fColor + 1) / 2) * 255;

				// get color from palette
				currentColor = palette[PApplet.parseInt(fColor)];

				// colorize current pixel
				currentPixel = (y * width) + x;
				pixels[currentPixel] = currentColor;
			}
		}
		updatePixels();
		time++;
	}

	static public void main(String args[]) {
		PApplet.main(new String[] { "--bgcolor=#FFFFFF", Plasma.class.getName()});
	}
}