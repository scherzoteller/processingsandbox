package org.scherzoteller.processing.sandbox;

import org.scherzoteller.processing.commons.ImageBuf;
import org.scherzoteller.utils.lambda.TriConsumer;

import processing.core.PApplet;

public class SpriteOnBackground extends PApplet {
	private ImageBuf sprite;
	private ImageBuf background;
	private int lastSpX = -1, lastSpY = -1;
	private int spX = 0, spY = 0;
	int time;
	
	@Override
	public void settings() {
		super.settings();
		size(1280, 1024, P2D);
	}

	public void setup() {
		noStroke();
		this.sprite = new ImageBuf("lb.png");
	}
	
	private int getColorInTile(int x, int y, ImageBuf tile){
		int tiHeight = tile.height;
		int tiWidth = tile.width;

		int xInTile = x%tiWidth;
		int yInTile = y%tiHeight;
		int color = tile.getPixel(xInTile, yInTile);
		return color;
	}
	
	private void tileBackground(String filename) {
		this.background = new ImageBuf(filename);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int currentPixel = getCurrentPixelNoClip(y, x);
				pixels[currentPixel]=getColorInTile(x, y, background);
			}
		}

	}	

	@Override
	public void loadPixels() {
		boolean init = pixels == null;
		super.loadPixels();
		if(init){
			tileBackground("pattern.png");
		}
	}
	
	public void draw() {
		loadPixels();
		drawSprite();
		move();
		updatePixels();
	}

	private void drawSprite() {
		int spHeight = this.sprite.height;
		int spWidth = this.sprite.width;

		TriConsumer<Integer, Integer, Integer> transparencyAction = (x,y,currentPixel) ->pixels[currentPixel] = getColorInTile(x%width, y%height, background);
		if (lastSpX >= 0) {
			if(spX == 0 || spY == 0){
				// Full sprite erase
				for (int y = lastSpY; y < lastSpY + spHeight; y++) {
					for (int x = lastSpX; x < lastSpX + spWidth; x++) {
						int currentPixel = getCurrentPixel(y, x);
						transparencyAction.accept(x,y,currentPixel);
					}
				}
			}else{
				// Partial sprite erase
				for (int y = lastSpY; y < lastSpY + ytranslate; y++) {
					for (int x = lastSpX; x < lastSpX + spWidth; x++) {
						int currentPixel = getCurrentPixel(y, x);
						transparencyAction.accept(x,y,currentPixel);
					}
				}
				for (int y = lastSpY+ytranslate; y < lastSpY + spHeight; y++) {
					for (int x = lastSpX; x < lastSpX + xtranslate; x++) {
						int currentPixel = getCurrentPixel(y, x);
						transparencyAction.accept(x,y,currentPixel);
					}
				}
			}
		}

		// Draw new sprite
		drawImage(sprite, spX, spY, transparencyAction );
	}
	
	private void drawImage(ImageBuf image, int offX, int offY, TriConsumer<Integer, Integer, Integer> transparencyAction){
		int imgHeight = image.height;
		int imgWidth = image.width;
		
		for (int y = 0; y < imgHeight; y++) {
			// draw line
			for (int x = 0; x < imgWidth; x++) {
				int currentPixel =   getCurrentPixel(offY+y, offX+x);
				int color = image.getPixel(x, y); 
				if (color != 0) {
					pixels[currentPixel] = color;
				} else {
					transparencyAction.accept(offX+x,offY+y,currentPixel);
				}
			}
		}
	}
	
	

	private int getCurrentPixel(int y, int x) {
		y = y%height;
		x = x%width;
		return getCurrentPixelNoClip(y,x);
	}
	
	private int getCurrentPixelNoClip(int y, int x) {
		return (y * width) + x;
	}
	
	
	final int xtranslate = 2;
	final int ytranslate = 2;

	private void move() {
		lastSpX = spX;
		spX += xtranslate;
		
		if (spX > width) {
			spX = 0;
		}
		lastSpY = spY;
		spY += ytranslate;
		if (spY > height) {
			spY = 0;
		}
		time++;
	}

	static public void main(String args[]) {
		PApplet.main(new String[] { SpriteOnBackground.class.getName() });
	}
}