package org.scherzoteller.processing.sandbox;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import processing.core.PApplet;

public class Fading extends PApplet {
	private int[][] spPixels;
	private int spX = 0, spY = 0;

	@Override
	public void settings() {
		super.settings();
		size(1280, 1024, P2D);
	}

	private final int BACKGROUND = color(0, 0, 0);

	public void setup() {
		background(BACKGROUND);
		noStroke();
		this.spPixels = readImage("lb.png");
	}
	
	
	private int[][] readImage(final String spriteFilename) {
		try {
			BufferedImage image;
			image = ImageIO.read(new File(this.getClass().getResource("/"+spriteFilename).getFile()));
			return convertTo2DWithoutUsingGetRGB(image);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	

	int time;
	
	@Override
	public void loadPixels() {
		super.loadPixels();
	}
	

	public void draw() {
		loadPixels();
		drawSprite();
		move();
		updatePixels();
	}

	/**
	 * Note that image is reverted pixel should be accessed as resultArray[y][x]
	 * FIXME can be sometimes buggy: see ko_pattern.png
	 * @param image
	 * @return
	 */
	private int[][] convertTo2DWithoutUsingGetRGB(BufferedImage image) {
		final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;

		int[][] result = new int[height][width];
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0, row = 0, col = 0; pixel + 3 < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
				argb += ((int) pixels[pixel + 1] & 0xff); // blue
				argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
				argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0, row = 0, col = 0; pixel + 2 < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += -16777216; // 255 alpha
				argb += ((int) pixels[pixel] & 0xff); // blue
				argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
				argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		}
		return result;
	}
	
	
	private void drawSprite() {
		Arrays.fill(pixels, 0);
		
		// Draw new sprite
		drawImage(spPixels, spX, spY/*, transparencyAction */, space);
		
	}
	
	
	private int fadeFunc(int color){
		int b = (color & 0xff) * fadePercent / 100;
		int g = ((color >> 8) & 0xff) * fadePercent / 100;
		int r = ((color >> 16) & 0xff) * fadePercent / 100;
		int a = ((color >> 24) & 0xff);
		
		int argb = 0;
		argb += ((int) b & 0xff); // blue
		argb += (((int) g & 0xff) << 8); // green
		argb += (((int) r & 0xff) << 16); // red
		argb += (((int) a & 0xff) << 24); // alpha
		return argb;
	}
	
	
	private void drawImage(int[][] image, int offX, int offY/*, TriConsumer<Integer, Integer, Integer> transparencyAction*/, int space){
		int imgHeight = image.length;
		int imgWidth = image[0].length;
		
		for (int y = 0; y < imgHeight; y++) {
			// draw line
			for (int x = 0; x < imgWidth; x++) {
				int currentPixel =   getCurrentPixel(offY+(y*space), offX+(x*space));
				int color = image[y][x]; 
				if (color != 0) {
					pixels[currentPixel] = fadeFunc(color);
				}
			}
		}
	}
	
	

	private int getCurrentPixel(int y, int x) {
		y = y%height;
		x = x%width;
		return getCurrentPixelNoClip(y,x);
	}
	
	private int getCurrentPixelNoClip(int y, int x) {
		return (y * width) + x;
	}
	
	
	final int xtranslate = 2;
	final int ytranslate = 2;

	private final int initSpace = 1;
	private int space = initSpace;
	
	private int fadePercent = 0;
	private boolean revert = false;
	
	private void move() {
		spX += xtranslate;
		
		if (spX > width) {
			spX = 0;
		}
		spY += ytranslate;
		if (spY > height) {
			spY = 0;
		}
		
		revert = (revert || fadePercent == 100) && fadePercent != 0;
		if(revert){
			fadePercent--;
		}else{
			fadePercent++;
		}
		
		time++;
	}

	static public void main(String args[]) {
		PApplet.main(new String[] { Fading.class.getName() });
	}
}