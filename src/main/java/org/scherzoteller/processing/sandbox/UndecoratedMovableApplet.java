package org.scherzoteller.processing.sandbox;

import java.awt.Frame;

import processing.awt.PSurfaceAWT;
import processing.awt.PSurfaceAWT.SmoothCanvas;
import processing.core.PApplet;
import processing.core.PSurface;
import processing.event.KeyEvent;

/**
 * 
 * P3D can allow lines with Z: https://processing.org/reference/size_.html
 * 
 *
 */
public class UndecoratedMovableApplet extends PApplet
{
	private Frame frame;
	@Override
	protected PSurface initSurface() {
		PSurface pSurface = super.initSurface();
		PSurfaceAWT awtSurface = (PSurfaceAWT) surface;
		SmoothCanvas smoothCanvas = (SmoothCanvas) awtSurface.getNative();
		frame = smoothCanvas.getFrame();
		frame.setUndecorated(true);
		return pSurface;
	}
	
	
	@Override
	public void keyPressed(KeyEvent event) {
		super.keyPressed(event);
		int offset = 10;
		boolean ctrl = (event.getModifiers()&java.awt.event.KeyEvent.CTRL_MASK) != 0;
		boolean alt = (event.getModifiers()&java.awt.event.KeyEvent.ALT_MASK) != 0;
		if(ctrl){
			if(alt){
				offset = 500;
			}else{
				offset = 100;
			}
		}
		if(event.getKeyCode() == java.awt.event.KeyEvent.VK_LEFT){
			frame.setLocation(frame.getLocation().x-offset, frame.getLocation().y);
		}else if(event.getKeyCode() == java.awt.event.KeyEvent.VK_RIGHT){
			frame.setLocation(frame.getLocation().x+offset, frame.getLocation().y);
		}else if(event.getKeyCode() == java.awt.event.KeyEvent.VK_UP){
			frame.setLocation(frame.getLocation().x, frame.getLocation().y-offset);
		}else if(event.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN){
			frame.setLocation(frame.getLocation().x, frame.getLocation().y+offset);
		}else if(event.getKeyCode() == java.awt.event.KeyEvent.VK_HOME){
			frame.setLocation(-1790, 8);
		}else if(event.getKeyCode() == java.awt.event.KeyEvent.VK_END){
			frame.setLocation(110, 8);
		}
		System.err.println("x="+frame.getLocation().x+" y="+frame.getLocation().y);
	}
}