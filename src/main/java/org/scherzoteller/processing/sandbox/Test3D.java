package org.scherzoteller.processing.sandbox;

import processing.core.PApplet;

public class Test3D extends PApplet {
	private static final int PALETTE_SIZE = 256;
//	float x, y, z;
	int palette[] = new int[PALETTE_SIZE]; // Un tablean de 256 couleurs

	float xMult, zMult, yMult;
	int colorIdx = 0;
	

	@Override
	public void settings() {
		super.settings();
		size(1000, 1000, P3D);
	}

	public void setup() {
//		size(200, 200, P3D);
		background(100);
		rectMode(CENTER);
		fill(51);
		stroke(255);
		initPalette();

	}

	private void initPalette() {
		// create color palette
		int i, r, g, b;
		// between 0..100 fade black to red
		for (i = 0; i < 100; i++) {
			r = i * 255 / 100;
			g = 0;
			b = 0;
			palette[i] = color(r, g, b);
		}
		// between 100..150 fade red to yellow
		for (; i < 150; i++) {
			r = 255;
			g = (i - 100) * 255 / 50;
			b = 0;
			palette[i] = color(r, g, b);
		}
		// between 150..255 fade yellow to blue
		for (; i <= 255; i++) {
			r = 255 - ((i - 150) * 255 / 106);
			g = 255 - ((i - 150) * 255 / 106);
			b = (i - 150) * 255 / 106;
			palette[i] = color(r, g, b);
		}
	}

	public void draw() {
		translate(500, 500, 0);
		moveLinear();
		fill(palette[colorIdx]);
		rect(0, 0, 300, 300);
		colorIdx = (colorIdx+1)%PALETTE_SIZE;
	}

	private void move() {
		rotateZ(PI/8 * zMult);
		rotateX(PI/4 * xMult);
		rotateY(PI/8 * yMult);
		xMult = xMult+(PI/18)%(2*PI);
		zMult = zMult+(PI/9)%(2*PI);
		yMult = yMult+(PI/6)%(2*PI);
	}

	private void moveLinear() {
		rotateZ(PI/8 * zMult);
		rotateX(PI/8 * xMult);
		rotateY(PI/8 * yMult);
		xMult = xMult+(PI/18)%(2*PI);
		zMult = zMult+(PI/18)%(2*PI);
		yMult = yMult+(PI/18)%(2*PI);
	}
	
	static public void main(String args[]) {
		PApplet.main(new String[] { Test3D.class.getName() });
	}
}