package org.scherzoteller.processing.sandbox;

import java.util.Arrays;

import org.scherzoteller.processing.commons.BlurableImageBuf;
import org.scherzoteller.processing.commons.ImageBuf;

import processing.core.PApplet;

public class Blurry extends PApplet {
	private BlurableImageBuf sprite;
	private int spX = 0, spY = 0;

	@Override
	public void settings() {
		super.settings();
		//size(1280, 1024, P2D);
		fullScreen(P2D);
	}

	private final int BACKGROUND = color(0, 0, 0);

	public void setup() {
		background(BACKGROUND);
		noStroke();
		this.sprite = new BlurableImageBuf("lb.png");
	}
	
	

	int time;
	
	@Override
	public void loadPixels() {
		super.loadPixels();
	}
	

	public void draw() {
		loadPixels();
		applyBlur();
		drawSprite();
		move();
		updatePixels();
	}
	private void applyBlur() {
		if(blurLevel == 0){
			this.sprite = new BlurableImageBuf("lb.png");
		}else{
			this.sprite.blur();
		}
	}

	
	private void drawSprite() {
		Arrays.fill(pixels, 0);
		
		// Draw new sprite
		drawImage(sprite, spX, spY/*, transparencyAction */, space);
		
	}
	
	
	private void drawImage(ImageBuf image, int offX, int offY/*, TriConsumer<Integer, Integer, Integer> transparencyAction*/, int space){
		int imgHeight = image.height;
		int imgWidth = image.width;
		
		for (int y = 0; y < imgHeight; y++) {
			// draw line
			for (int x = 0; x < imgWidth; x++) {
				int currentPixel =   getCurrentPixel(offY+(y*space), offX+(x*space));
				int color =  image.getPixel(x, y); 
				if (color != 0) {
					pixels[currentPixel] = color;
				}
			}
		}
	}
	
	

	private int getCurrentPixel(int y, int x) {
		y = y%height;
		x = x%width;
		return getCurrentPixelNoClip(y,x);
	}
	
	private int getCurrentPixelNoClip(int y, int x) {
		return ImageBuf.getPixelIndex(y, x, width);
	}
	
	
	final int xtranslate = 2;
	final int ytranslate = 2;

	private final int initSpace = 1;
	private int space = initSpace;
	
	private int blurLevel;
	
	private void move() {
		spX += xtranslate;

		if (spX > width) {
			spX = 0;
		}
		spY += ytranslate;
		if (spY > height) {
			spY = 0;
		}
		
		blurLevel = time%30;

		time++;
	}

	static public void main(String args[]) {
		PApplet.main(new String[] { Blurry.class.getName() });
	}
}